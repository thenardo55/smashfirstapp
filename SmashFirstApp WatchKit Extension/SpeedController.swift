//
//  SpeedController.swift
//  SmashFirstApp WatchKit Extension
//
//  Created by Thenardo Ardo on 25/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import Foundation
import CoreLocation

class SpeedController: WKInterfaceController, CLLocationManagerDelegate {
    
    @IBOutlet weak var speedLabel: WKInterfaceLabel!
    
    let locationManager = CLLocationManager()
    var speedPerformance = CLLocationSpeed()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 0.1
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        speedLabel.setText("0.0 m/s")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        locationManager.startUpdatingLocation()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
        locationManager.stopUpdatingLocation()
        speedLabel.setText("\(speedPerformance) m/s")
    }
    
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        speedPerformance = location.speed
        print(location)
        speedLabel.setText("\(speedPerformance) m/s")
    }
    
}
