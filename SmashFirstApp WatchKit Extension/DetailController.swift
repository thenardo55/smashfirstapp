//
//  DetailController.swift
//  SmashFirstApp WatchKit Extension
//
//  Created by Thenardo Ardo on 17/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import WatchConnectivity
import CoreMotion
import HealthKit
import Foundation

class DetailController: WKInterfaceController, WCSessionDelegate, HKWorkoutSessionDelegate {
    
    @IBOutlet weak var toggleAcceleratorCaptureButton: WKInterfaceButton!
    @IBOutlet weak var accLabelX: WKInterfaceLabel!
    @IBOutlet weak var accLabelY: WKInterfaceLabel!
    @IBOutlet weak var accLabelZ: WKInterfaceLabel!
    
    var session: WCSession!
    var motionManager: CMMotionManager!
    let healthStore = HKHealthStore()
    var workoutSession: HKWorkoutSession!
    var isWorkoutActive = false
    var repeatAccTimer: Timer!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        toggleAcceleratorCaptureButton.setTitle("Start")
        
        motionManager = CMMotionManager()
        motionManager.accelerometerUpdateInterval = 1.0 / 60.0
        motionManager.startAccelerometerUpdates()
        
        if WCSession.isSupported() {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        
        //updateAcceleratorData()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        //updateAcceleratorData()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func willDisappear() {
        super.willDisappear()
        
        //repeatAccTimer?.invalidate()
    }
    
    @objc func updateAcceleratorData() {
        
        if let motionData = motionManager.accelerometerData {
            let t_stamp = getTimestamp()
            let x_val = motionData.acceleration.x
            let y_val = motionData.acceleration.y
            let z_val = motionData.acceleration.z
            
            accLabelX.setText("\(x_val)")
            accLabelY.setText("\(y_val)")
            accLabelZ.setText("\(z_val)")
            
            let data: [String: Any] = ["watch_tstamp": "\(t_stamp)", "watch_x": "\(x_val)", "watch_y": "\(y_val)", "watch_z": "\(z_val)" as Any]
            session.transferUserInfo(data)
        }
    }
    
    func getTimestamp() -> String {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        return formattedDate
    }
    
    // MARK: - WCSession Delegate
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //
    }
    
    // MARK: - HKWorkoutSession Delegate
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        //
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        //
    }
    
    // MARK: - Action Buttons
    
    @IBAction func toggleAcceleratorCapture() {
        if isWorkoutActive {
            toggleAcceleratorCaptureButton.setTitle("Start")
            isWorkoutActive = false
            
            repeatAccTimer.invalidate()
            workoutSession.end()
        } else {
            toggleAcceleratorCaptureButton.setTitle("Stop")
            isWorkoutActive = true
            
            let workoutConfig = HKWorkoutConfiguration()
            workoutConfig.activityType = .badminton
            workoutConfig.locationType = .unknown
            
            do {
                workoutSession = try HKWorkoutSession(healthStore: healthStore, configuration: workoutConfig)
                workoutSession.delegate = self
                workoutSession.startActivity(with: Date())
                
                self.repeatAccTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateAcceleratorData), userInfo: nil, repeats: true)
            } catch let error as NSError {
                // error
                print(error)
            }
            
            
        }
    }
    
}
