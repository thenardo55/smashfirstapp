//
//  SecondController.swift
//  SmashFirstApp WatchKit Extension
//
//  Created by Thenardo Ardo on 17/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import HealthKit
import Foundation

class SecondController: WKInterfaceController {
    
    @IBOutlet weak var statusRing: WKInterfaceActivityRing!

    let summary = HKActivitySummary()
    let value: Double = 2
    let goal: Double = 10
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        summary.activeEnergyBurned = HKQuantity(unit: HKUnit.kilocalorie(), doubleValue: 2.5)
        summary.activeEnergyBurnedGoal = HKQuantity(unit: HKUnit.kilocalorie(), doubleValue: 10)
        
        summary.appleExerciseTime = HKQuantity(unit: HKUnit.minute(), doubleValue: 4.6)
        summary.appleExerciseTimeGoal = HKQuantity(unit: HKUnit.minute(), doubleValue: 10)
        
        summary.appleStandHours = HKQuantity(unit: HKUnit.count(), doubleValue: 8.8)
        summary.appleStandHoursGoal = HKQuantity(unit: HKUnit.count(), doubleValue: 10)
        
        statusRing.setActivitySummary(summary, animated: true)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
