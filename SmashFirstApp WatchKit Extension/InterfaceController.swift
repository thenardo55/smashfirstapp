//
//  InterfaceController.swift
//  SmashFirstApp WatchKit Extension
//
//  Created by Thenardo Ardo on 17/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {

    @IBOutlet weak var messageLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        messageLabel.setText("SmashCourt")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
