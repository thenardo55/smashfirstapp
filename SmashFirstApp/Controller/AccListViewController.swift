//
//  AccListViewController.swift
//  SmashFirstApp
//
//  Created by Thenardo Ardo on 19/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class AccListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var accDataTable: UITableView!
    
    var arrOfAcc = [AccCoreData]()
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadItems()
        
        accDataTable.dataSource = self
        accDataTable.delegate = self
    }
    
    func loadItems() {
        let request: NSFetchRequest<AccCoreData> = AccCoreData.fetchRequest()
        do {
            arrOfAcc = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan daftar Accelerator")
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfAcc.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "accTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? AccTableViewCell else {
                fatalError("The dequened cell is not an instance of AccTableViewCell")
        }
        
        let acc = arrOfAcc[indexPath.row]
        
        cell.tstampLabel.text = acc.timestamp
        cell.xDataLabel.text = acc.x
        cell.yDataLabel.text = acc.y
        cell.zDataLabel.text = acc.z
        
        return cell
    }
    
    // MARK: - Action Buttons
    
    @IBAction func exportAction(_ sender: UIButton) {
        let exportedFileName = "AcceleratorBadmintonPlayerData_\(Date()).csv"
        let exportedPath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(exportedFileName)!
        var exportedCsvText = "DateTime,x,y,z\n"
        
        if arrOfAcc.count > 0 {
            
            for acc in arrOfAcc {
                let exportedLine = "\(acc.timestamp ?? ""),\(acc.x ?? ""),\(acc.y ?? ""),\(acc.z ?? "")\n"
                exportedCsvText.append(contentsOf: exportedLine)
            }
            
            do {
                try exportedCsvText.write(to: exportedPath, atomically: true, encoding: String.Encoding.utf8)
                
                let vc = UIActivityViewController(activityItems: [exportedPath], applicationActivities: [])
                vc.excludedActivityTypes = [
                    UIActivity.ActivityType.assignToContact,
                    UIActivity.ActivityType.saveToCameraRoll,
                    UIActivity.ActivityType.postToFlickr,
                    UIActivity.ActivityType.postToVimeo,
                    UIActivity.ActivityType.postToTencentWeibo,
                    UIActivity.ActivityType.postToTwitter,
                    UIActivity.ActivityType.postToFacebook,
                    UIActivity.ActivityType.openInIBooks
                ]
                present(vc, animated: true, completion: nil)
            } catch {
                let alert = UIAlertController(title: "Gagal export", message: "Terjadi kesalahan ketika melakukan export", preferredStyle: .alert)
                let okAlert = UIAlertAction(title: "OK", style: .cancel, handler: { (action) -> Void in })
                alert.addAction(okAlert)
                present(alert, animated: true, completion: nil)
            }
            
        } else {
            // tidak ada data untuk di export
            let alert = UIAlertController(title: "Tidak ada data", message: "Tidak ada data untuk di export", preferredStyle: .alert)
            let okAlert = UIAlertAction(title: "OK", style: .cancel, handler: { (action) -> Void in })
            alert.addAction(okAlert)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
