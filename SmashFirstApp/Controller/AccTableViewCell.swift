//
//  AccTableViewCell.swift
//  SmashFirstApp
//
//  Created by Thenardo Ardo on 19/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class AccTableViewCell: UITableViewCell {

    @IBOutlet weak var tstampLabel: UILabel!
    @IBOutlet weak var xDataLabel: UILabel!
    @IBOutlet weak var yDataLabel: UILabel!
    @IBOutlet weak var zDataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
