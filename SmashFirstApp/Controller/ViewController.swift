//
//  ViewController.swift
//  SmashFirstApp
//
//  Created by Thenardo Ardo on 17/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var tstampLabel: UILabel!
    @IBOutlet weak var xDataLabel: UILabel!
    @IBOutlet weak var yDataLabel: UILabel!
    @IBOutlet weak var zDataLabel: UILabel!
    @IBOutlet weak var timesReceived: UILabel!
    
    var arrOfAcc = [AccCoreData]()
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    var session: WCSession!
    var receivedTimes: Int = 0
    
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        timesReceived.text = "\(receivedTimes)"
        self.configureWatchKitSession()
        loadItems()
        //determineMyCurrentLocation()
    }
    
    func configureWatchKitSession() {
        if WCSession.isSupported() {
            session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    func saveItems() {
        do {
            try context.save()
        } catch  {
            print("Gagal menyimpan data ke GroupCoreData")
        }
    }
    
    func loadItems() {
        let request: NSFetchRequest<AccCoreData> = AccCoreData.fetchRequest()
        do {
            arrOfAcc = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan daftar Accelerator")
        }
    }
    
    func saveFetchedAccData(tstamp: String, x: String, y: String, z: String) {
        let newAcc = AccCoreData(context: self.context)
        newAcc.timestamp = tstamp
        newAcc.x = x
        newAcc.y = y
        newAcc.z = z
        arrOfAcc.append(newAcc)
        saveItems()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        print("user speed = \(userLocation.speed)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    // MARK: - WCSession Delegate
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        DispatchQueue.main.async {
            let fetchedTimestamp = userInfo["watch_tstamp"] as? String
            let fetchedX = userInfo["watch_x"] as? String
            let fetchedY = userInfo["watch_y"] as? String
            let fetchedZ = userInfo["watch_z"] as? String
            self.tstampLabel.text = fetchedTimestamp
            self.xDataLabel.text = "x: " + (fetchedX ?? "")
            self.yDataLabel.text = "y: " + (fetchedY ?? "")
            self.zDataLabel.text = "z: " + (fetchedZ ?? "")
            //self.receivedTimes = self.receivedTimes + 1
            //self.timesReceived.text = "\(self.receivedTimes)"
            self.saveFetchedAccData(tstamp: fetchedTimestamp!, x: fetchedX!, y: fetchedY!, z: fetchedZ!)
        }
    }
    
}
