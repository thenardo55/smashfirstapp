//
//  AccModel.swift
//  SmashFirstApp
//
//  Created by Thenardo Ardo on 19/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

struct AccModel {
    var timestamp: String
    var x: String
    var y: String
    var z: String
}
